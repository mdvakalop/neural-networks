Movie recommendation algorithm using the summaries of each movie based on context.<br/> We used each word in the summary to find similarities between the movies with the help of TFIDF representation. The dataset used was Carnegie Mellon Movie Summary Corpus. We also did Semantic Data visualization using SOM (Self organizing maps).

