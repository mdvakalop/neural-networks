Development of a movie recommendation system. <br/>
Use of machine learning algorithms for classification. <br/>
Use of genetic algorithms to solve - approximate functions.<br/>
Developed by [Myrsini Vakalopoulou ](https://gitlab.com/mdvakalop) and [Georgios Hadjiharalambous](https://gitlab.com/Georgios.Hadjiharalambous)